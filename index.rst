
.. raw:: html

   <a rel="me" href="https://kolektiva.social/@raar"></a>
   <a rel="me" href="https://kolektiva.social/@noamsw"></a>


.. un·e
.. https://framapiaf.org/web/tags/raar.rss
.. https://framapiaf.org/web/tags/golem.rss

|FluxWeb| `RSS <https://cbl.frama.io/cbl-grenoble/linkertree/rss.xml>`_

.. _linkertree:
.. _links:

================================================================================
Liens **Cercle Bernard Lazare Grenoble** |CblGrenoble|
================================================================================

- https://www.cbl-grenoble.org/8-cbl-grenoble-0-action-0.html (site d'origine)


Cercle Bernard Lazare Grenoble
=================================

- https://www.cbl-grenoble.org/
- https://www.helloasso.com/associations/cercle-bernard-lazare-grenoble
- https://cbl.frama.io/cbl-grenoble/decennie-2020
- https://cbl.frama.io/cbl-grenoble/infos

Associations
=================

- `Association pour un Judaïsme Humaniste et Laïc <http://www.ajhl.org/>`_
- `Communauté juive libérale de Grenoble <http://cjl-grenoble.org/>`_
- `les Amis du Monde Diplomatique <https://www.amis.monde-diplomatique.fr/>`_
- `Le Cercle Bernard Lazare - Paris <https://www.cerclebernardlazare.org/>`_
- `Convergence Internationale <http://convergenceint.free.fr/>`_
- `Cercle Martin Buber <http://www.cerclemartinbuber.ch/wordpress/>`_
- `Liberté du Judaïsme <https://www.facebook.com/people/Libert%C3%A9-du-juda%C3%AFsme/100069848370079/>`_
- `Le Crearc <http://crearc.fr/>`_
- `Amal <https://www.amal38.fr/page/1790594-accueil>`_

Antiracisme et Droits de l'Homme
========================================

- `Association de Parrainage Républicain des Demandeurs d'Asile et de Protection <www.apardap.org>`_
- Nous les prenons sous notre protection : Education sans frontières
- `La Ligue des Droits de l'Homme <https://www.ldh-france.org/>`_
- Amnesty - Antenne Campus
- `Amnesty International <https://www.amnesty.fr/>`_
- `L'Ecole de la Paix <http://www.ecoledelapaix.org>`_
- `SOS-Racisme <https://sos-racisme.org/>`_

Institutions
=======================



- :ref:`Le Musée de la Résistance et de la Déportation de l'Isère <resistance_gre:resistance_gre>`
- `Les Bibliothèques de Grenoble <https://bm-grenoble.fr/>`_
- `Le Musée Dauphinois <https://musees.isere.fr/musee/musee-dauphinois>`_
- L'Université Stendhal
- `La Ville de Grenoble <https://www.grenoble.fr/index.php>`_
- `La Maison d'Izieu <https://www.memorializieu.eu/d%C3%A9couvrir/presentation-generale/>`_

Israël et Proche-Orient
===============================

- Israel/Palestine Center for Research and Information
- `La Paix Maintenant Shalom Akhshav <https://www.lapaixmaintenant.org/>`_
- `Initiative de Genève <https://www.lapaixmaintenant.org/Les-Accords-de-Geneve/>`_

Musique, Photos, Festivals
=================================

- `Chants yiddish et photos de Paris, Grenoble, Berlin <http://www.chambre-claire.com/>`_
- `Festivals : tous les festivals en Rhône-Alpes <https://www.concertandco.com/lfestival.php?rg=RHO>`_
- `Eliane Aberdam, compositeur <https://composer.aberdam.com/>`_
- `Diasporim Zinger <https://www.diasporim-zinger.com/>`_
- `Hotegezugt <http://www.borzy.info/>`_


